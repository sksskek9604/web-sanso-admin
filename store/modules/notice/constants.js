export default {
    DO_NOTICE_LIST_DATE: 'notice/doNoticeListDate',
    DO_NOTICE_LIST_ALL: 'notice/doNoticeListAll',
    DO_NOTICE_LIST_NOTE_ALL: 'notice/doNoticeListNoteAll',
    DO_NOTICE_DETAIL:'notice/doNoticeDetail',
    DO_NOTICE_UPDATE:'notice/doNoticeUpdate',

    DO_CREATE: 'notice/doCreate',

    FETCH_NOTICE_LIST_ALL: 'notice/fetchNoticeListAll',

    GET_NOTICE_LIST_ALL: 'notice/getNoticeListAll'

    // DO_DETAIL: 'test-data/doDetail',
    // DO_UPDATE: 'test-data/doUpdate',
    // DO_DELETE: 'test-data/doDelete',
}
