const BASE_URL = '/v1/notice'

export default {

    DO_CREATE: `${BASE_URL}/new`, //post

    DO_NOTICE_LIST_DATE: `${BASE_URL}/list/page/{pageNum}`, //post
    DO_NOTICE_LIST_ALL: `${BASE_URL}/list/all`, //get
    DO_NOTICE_LIST_NOTE_ALL: `${BASE_URL}/list/note/all`, //get
    DO_NOTICE_DETAIL:  `${BASE_URL}/detail/{noticeId}`,

    DO_NOTICE_UPDATE: `${BASE_URL}/put/{noticeId}`, //put

    // url만 담당하는 곳이기 때문에 FETCH가 올 수 없음.
}
